//
//  CatalogViewController.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 15/06/2021.
//

import UIKit

///
/// The Catalog view controller displays a screen with the most popular movies
///
class CatalogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    fileprivate var tableView: UITableView!
    fileprivate var searchBar: UISearchBar!

    fileprivate var viewModel: MoviesViewModel
    
    fileprivate var keyboardAdjusted = false
    fileprivate var searchFieldBottomConstraint: NSLayoutConstraint?
    
    init(movieAPI api: MovieAPI) {
        
        viewModel = MoviesViewModel(movieAPI:api)
        
        super.init(nibName:nil, bundle:nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        buildInterface()

        /// Loads the popular movies and launch an update handling of its properties
        viewModel.loadMovies(loadHandler:moviesLoaded, updateHandler:movieUpdated)
        
        tableView.register(MyTableViewCell.self, forCellReuseIdentifier:"mycell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Deselect row when returning from detail view
        if let selectedRow = tableView.indexPathForSelectedRow {
            
            tableView.deselectRow(at: selectedRow, animated: true)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }

    /// Completion handler called by the movie view-model when all movies has been loaded.
    fileprivate func moviesLoaded(error: Error?) {
        if let err = error {
            displayError(err.localizedDescription)
        }
        else {
            tableView.reloadData()
        }
    }
    
    /// Completion handler called by the movie view-model when all movies has been retrieved and all properties populated
    fileprivate func movieUpdated(result: Result<MovieViewModel,Error>) {
        
        switch result {
            case .success(let image):
                if let i = viewModel.movies.firstIndex(where: { $0 === image }) {
                    tableView.reloadRows(at:[IndexPath(indexes:[0,i])], with:.automatic)
                }
            case .failure(let error):
                displayError(error.localizedDescription)
        }
    }
}

///
/// Extension with functions implementing delegate UITableViewDelegate
///
extension CatalogViewController {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"mycell", for:indexPath) as! MyTableViewCell
            
        cell.movieTextLabel.text = viewModel.movies[indexPath.row].title
        cell.movieImageView.image = viewModel.movies[indexPath.row].posterImage
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(MyTableViewCell.rowHeight)
    }
}

/// Extension with functions implementing delegate UITableViewDataSource
///
/// - Note: Table is configured by one section default
///
extension CatalogViewController {

    // Returns how many rows there are in the table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.movies.count
    }
    
    // Navigate on to the detail view, when a row is clicked at
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        showMovieDetailViewController(movie:viewModel.movies[indexPath.row])
    }
}

///
/// Extension with functions implementing delegate UISearchBarDelegate
///
extension CatalogViewController {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let searchText = searchBar.text, searchText.count > 0 {
            viewModel.search(searchText:searchText)
        }
        else {
            // This is actually not possible, as the keyboard search/enter button is disabled in that case
            viewModel.searchCancel()
        }
        
        tableView.reloadData()
        
        searchBar.endEditing(true)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        searchBar.text = ""
        
        viewModel.searchCancel()

        tableView.reloadData()

        searchBar.endEditing(true)
    }
}

/// Extension with helper functions to control keyboard does not hide search field
extension CatalogViewController {
    
    @objc fileprivate func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            searchFieldBottomConstraint!.constant = -keyboardSize.cgRectValue.height
        }
        searchBar.showsCancelButton = true
    }
    
    @objc fileprivate func keyboardWillHide(notification: NSNotification) {
        searchFieldBottomConstraint!.constant = 0
        searchBar.showsCancelButton = false
    }
}

/// Extension with helper functions
extension CatalogViewController {
    
    // Helper function to show detail view controller
    fileprivate func showMovieDetailViewController(movie: MovieViewModel) {
        
        let viewController = MovieDetailViewController(movie:movie)

        // Default title is a bit to long, so shorten it:
        let backItem = UIBarButtonItem()
        backItem.title = "Catalog"
        navigationItem.backBarButtonItem = backItem
        
        navigationController?.pushViewController(viewController, animated:true)
    }
}

///
/// Extension with functions used to programmatically build screen
///
extension CatalogViewController {
    
    /// Builds the screen programmatically
    fileprivate func buildInterface() {
        
        view.backgroundColor = .white
        
        // Table view
        
        tableView = UITableView()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier:"cellid")

        // Table view - constraints
        
        tableView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(tableView)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
        ])
        
        // Search bar
        
        searchBar = UISearchBar()

        searchBar.delegate = self
        
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(searchBar)
        
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            searchBar.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
        ])
        
        searchFieldBottomConstraint = searchBar.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor)
        searchFieldBottomConstraint!.isActive = true
        
        // Navigation controller
        
        navigationItem.title = "Movie Catalog"
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
    }
}
