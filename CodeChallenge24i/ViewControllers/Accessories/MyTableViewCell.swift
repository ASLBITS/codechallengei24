//
//  MyTableViewCell.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 17/06/2021.
//

import UIKit

///
/// Sub classing table view cell to get full control of the layout
///
class MyTableViewCell: UITableViewCell {
    
    static let rowHeight = Float(80) // See comments below (ASL)
    
    var movieImageView = UIImageView()
    var movieTextLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        movieImageView.translatesAutoresizingMaskIntoConstraints = false
        movieTextLabel.translatesAutoresizingMaskIntoConstraints = false
        
        movieImageView.contentMode = .scaleAspectFill
        movieImageView.clipsToBounds = true
        
        // Row height is determined by the font size and the maximum number of lines
        // Update static row height above, if something is changed (ASL)
        
        movieTextLabel.lineBreakMode = .byWordWrapping
        movieTextLabel.numberOfLines = 3
        movieTextLabel.font = UIFont.systemFont(ofSize:18, weight: .medium)
        
        addSubview(movieImageView)
        addSubview(movieTextLabel)
        
        NSLayoutConstraint.activate([
            movieImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            movieImageView.topAnchor.constraint(equalTo:self.topAnchor),
            movieImageView.bottomAnchor.constraint(equalTo:self.bottomAnchor),
            movieImageView.widthAnchor.constraint(equalTo: movieImageView.heightAnchor, multiplier:2, constant:0)
        ])
        
        NSLayoutConstraint.activate([
            movieTextLabel.leadingAnchor.constraint(equalTo: movieImageView.trailingAnchor, constant:10),
            movieTextLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            movieTextLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant:-10)
        ])
     }

     required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        assert(false)
    }
}
