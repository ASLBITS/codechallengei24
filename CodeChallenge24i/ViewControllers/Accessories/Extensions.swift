//
//  Extensions.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 18/06/2021.
//

import Foundation
import UIKit


/// Utility function to display a message in an UIViewController extension
extension UIViewController {

    func displayError(_ message: String) {
        
        assert(Thread.current.isMainThread)

        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil ))

        self.present(alert, animated:true, completion:nil)
    }
}


/// Utility functions to set up constraints
extension UIView {
    
    /// Utility function to set up a simple constraint between parent view and top view
    func setTopAndHorizontalConstraintsTo(view:UIView,topView:UIView, space:Double = 0) {
        
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: topView.bottomAnchor, constant:CGFloat(space)),
            view.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor),
        ])
    }
    
    /// Utility function to set up a simple constraints between parent view
    func setTLRBConstraintsWithMarginTo(view:UIView) {
        
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.layoutMarginsGuide.topAnchor),
            view.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor),
            view.bottomAnchor.constraint(equalTo: self.layoutMarginsGuide.bottomAnchor),
        ])
    }
    
    /// Utility function to set up a simple constraints between parent view
    func setTLRBConstraintsTo(view:UIView) {
        
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
}
