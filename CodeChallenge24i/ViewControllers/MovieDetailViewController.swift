//
//  MovieDetailViewController.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 16/06/2021.
//

import UIKit
import XCDYouTubeKit
import AVKit

///
/// The movie detail view controller displays a screen with details of a selected movie and a player button
///
class MovieDetailViewController: UIViewController {
    
    private var scrollView: UIScrollView!
    private var stackView: UIStackView!
    
    private var imageView: UIImageView!
    private var titleLabel: UILabel!
    private var playerButton: UIButton!
    private var genresTitleLabel: UILabel!
    private var genresLabel: UILabel!
    private var dateTitleLabel: UILabel!
    private var dateLabel: UILabel!
    private var overviewTitleLabel: UILabel!
    private var overviewLabel: UILabel!
    
    // Movie view model property, API Service and poster image are initialized by parent VC
    fileprivate let movie: MovieViewModel
    
    fileprivate var player: AVPlayerViewController!
    
    init(movie: MovieViewModel) {
        
        self.movie = movie

        super.init(nibName:nil, bundle:nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        player = AVPlayerViewController()
        
        // Notification on player finish used to dismiss player
        NotificationCenter.default.addObserver(self, selector: #selector(playerFinish),
                                               name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        buildInterface()
        populateInterface()
    }
    
    /// To get notified on orientation change
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        // View is still in old size until processing is done. Dispatch rebuild.
        DispatchQueue.main.async { [self] in
            buildInterface()
            populateInterface()
        }
    }
    
    /// Just to check that is released
    deinit {
        print("deinit \(self.description)")
    }

    @objc func playerButtonClickedAction() {
        
        print("playerButtonClickedAction, YouTube movie Id = \(movie.videoIdentifier ?? "")")
        
        present(player, animated: true, completion:nil)
        
        player.player = AVPlayer(url:URL(string:"http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8")!)
        
        player.player?.play()
    }
    
    @objc func playerFinish() {
        
        player.dismiss(animated:true, completion:nil)
    }
}

///
/// Extension with build screen code
///
extension MovieDetailViewController {
    
    static let space = 15.0
    
    var iPad: Bool { UIDevice.current.model.lowercased().contains("ipad") }
    
    /// Builds the screen programmatically
    fileprivate func buildInterface() {
        
        view.backgroundColor = .white
        
        navigationItem.title = "Movie Detail"
        
        if scrollView != nil {
            scrollView.removeFromSuperview()
        }
        
        scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isDirectionalLockEnabled = true
        
        view.addSubview(scrollView)
        
        view.setTLRBConstraintsWithMarginTo(view:scrollView)
        
        stackView = UIStackView()
        
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 0
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.addSubview(stackView)
        
        scrollView.setTLRBConstraintsTo(view:stackView)
        
        NSLayoutConstraint.activate([
            scrollView.widthAnchor.constraint(equalTo:stackView.widthAnchor)
        ])
        
        if UIDevice.current.orientation.isLandscape {
            buildInterfaceLandscape()
        }
        else {
            buildInterfacePortrait()
        }
    }
    
    fileprivate func buildInterfacePortrait() {
        
        buildInterfaceImageView()
        
        buildInterfaceSpace(scale:0.5)
        
        buildInterfaceTitleLabel()
        
        buildInterfaceSpace()
        
        buildInterfacePlayerButton()
        
        buildInterfaceSpace()
        
        buildInterfaceGenresTitleLabel()
        buildInterfaceGenresLabel()
        
        buildInterfaceSpace()
        
        buildInterfaceDateTitleLabel()
        buildInterfaceDateLabel()
        
        buildInterfaceSpace()
        
        buildInterfaceOverviewTitleLabel()
        buildInterfaceOverviewLabel()
    }
    
    fileprivate func buildInterfaceLandscape() {
        
        buildInterfaceImageTitlePlayer()
        
        buildInterfaceSpace(scale:0.5)
        
        buildInterfaceGenresTitleLabel()
        buildInterfaceGenresLabel()
        
        buildInterfaceSpace()
        
        buildInterfaceDateTitleLabel()
        buildInterfaceDateLabel()
        
        buildInterfaceSpace()
        
        buildInterfaceOverviewTitleLabel()
        buildInterfaceOverviewLabel()
    }
    
    fileprivate func buildInterfaceImageView() {
        
        let imageHeight = (iPad ? 0.5 : 0.333) * view.bounds.height
        
        imageView = UIImageView()
        
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.addArrangedSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant:CGFloat(imageHeight))
        ])
    }
    
    fileprivate func buildInterfaceTitleLabel() {
        
        titleLabel = UILabel()
        
        titleLabel.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize + 5)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.numberOfLines = 4
        titleLabel.lineBreakMode = .byWordWrapping
        
        stackView.addArrangedSubview(titleLabel)
    }
    
    fileprivate func buildInterfacePlayerButton() {
        
        let buttonHeight = 3 * UIFont.buttonFontSize
        
        playerButton = UIButton()
        
        playerButton.translatesAutoresizingMaskIntoConstraints = false
        playerButton.backgroundColor = UIColor.init(white:0.85, alpha:1)
        playerButton.setTitle("Watch Trailer", for: .normal)
        playerButton.setTitleColor(.black, for: .normal)
        
        stackView.addArrangedSubview(playerButton)
        
        NSLayoutConstraint.activate([
            playerButton.heightAnchor.constraint(equalToConstant:CGFloat(buttonHeight))
        ])
        
        playerButton.addTarget(self, action: #selector(playerButtonClickedAction), for: .touchUpInside)
    }
    
    fileprivate func buildInterfaceImageTitlePlayer() {
        
        imageView = UIImageView()
        
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        let imageHeight = (iPad ? 0.6 : 0.7) * view.bounds.height
        let imageWidth = 1.7 * imageHeight
        
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant:imageWidth)
        ])
        
        let imageTitlePlayerView = UIView()
        imageTitlePlayerView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageTitlePlayerView.heightAnchor.constraint(equalToConstant:imageHeight)
        ])
        
        stackView.addArrangedSubview(imageTitlePlayerView)
        imageTitlePlayerView.addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: imageTitlePlayerView.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: imageTitlePlayerView.leadingAnchor),
            imageView.bottomAnchor.constraint(equalTo: imageTitlePlayerView.bottomAnchor)
        ])
        
        titleLabel = UILabel()
        
        titleLabel.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize + 5)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.numberOfLines = 2
        titleLabel.textAlignment = .left
        
        imageTitlePlayerView.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: imageTitlePlayerView.topAnchor, constant:CGFloat(Self.space)),
            titleLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant:CGFloat(Self.space)),
            titleLabel.trailingAnchor.constraint(equalTo: imageTitlePlayerView.trailingAnchor, constant:CGFloat(-Self.space))
        ])
        
        let buttonHeight = 3 * UIFont.buttonFontSize
        
        playerButton = UIButton()
        
        playerButton.translatesAutoresizingMaskIntoConstraints = false
        playerButton.backgroundColor = UIColor.init(white:0.85, alpha:1)
        playerButton.setTitle("Watch Trailer", for: .normal)
        playerButton.setTitleColor(.black, for: .normal)
        
        imageTitlePlayerView.addSubview(playerButton)
        
        NSLayoutConstraint.activate([
            playerButton.trailingAnchor.constraint(equalTo: imageTitlePlayerView.trailingAnchor, constant:CGFloat(Self.space)),
            playerButton.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant:CGFloat(Self.space)),
            playerButton.bottomAnchor.constraint(equalTo: imageTitlePlayerView.bottomAnchor),
            playerButton.heightAnchor.constraint(equalToConstant:buttonHeight)
        ])
        
        playerButton.addTarget(self, action: #selector(playerButtonClickedAction), for: .touchUpInside)
    }
    
    fileprivate func buildInterfaceGenresTitleLabel() {
        
        genresTitleLabel = UILabel()
        
        genresTitleLabel.text = "Genres"
        
        genresTitleLabel.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
        genresTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.addArrangedSubview(genresTitleLabel)
    }
    
    fileprivate func buildInterfaceGenresLabel() {
        
        genresLabel = UILabel()
        
        genresLabel.translatesAutoresizingMaskIntoConstraints = false
        
        genresLabel.numberOfLines = 2
        
        stackView.addArrangedSubview(genresLabel)
    }
    
    fileprivate func buildInterfaceDateTitleLabel() {
        
        dateTitleLabel = UILabel()
        
        dateTitleLabel.text = "Date"
        
        dateTitleLabel.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
        dateTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.addArrangedSubview(dateTitleLabel)
    }
    
    fileprivate func buildInterfaceDateLabel() {
        
        dateLabel = UILabel()
        
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.addArrangedSubview(dateLabel)
    }
    
    fileprivate func buildInterfaceOverviewTitleLabel() {
        
        overviewTitleLabel = UILabel()
        
        overviewTitleLabel.text = "Overview"
        
        overviewTitleLabel.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
        overviewTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.addArrangedSubview(overviewTitleLabel)
    }
    
    fileprivate func buildInterfaceOverviewLabel() {
        
        overviewLabel = UILabel()
        
        overviewLabel.translatesAutoresizingMaskIntoConstraints = false
        
        overviewLabel.numberOfLines = 0
        
        stackView.addArrangedSubview(overviewLabel)
    }
    
    fileprivate func buildInterfaceSpace(scale:Float = 1.0) {
        
        let space = UIView()
        
        space.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.addArrangedSubview(space)
        
        NSLayoutConstraint.activate([
            space.heightAnchor.constraint(equalToConstant:CGFloat(scale)*UIFont.labelFontSize)
        ])
    }
    
    fileprivate func populateInterface() {
        
        titleLabel.text = movie.title
        
        dateLabel.text = movie.releaseDate
        overviewLabel.text = movie.overview
        imageView.image = movie.posterImage
        
        var text = ""
        
        for genre in movie.genres ?? [] {
            
            text += genre
            text += ", "
        }
        
        genresLabel.text = text.trimmingCharacters(in:[","," "])
    }
}
