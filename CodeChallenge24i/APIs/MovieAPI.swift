//
//  MovieAPI.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 17/06/2021.
//

import Foundation
import Alamofire
import UIKit

/// enum used in protocol MovieAPI
enum MediaObjectType {
    case SVG, PNG, JPG
}

///
/// API Errors, related to the MovieAPI protocol
///
/// - Note: See extension below for more description of errors
///
enum MovieAPIError: Swift.Error {
    
    case HTTPGetRequest(Error)
    case InvalidParameter(String)
    case MediaObjectLoad(String)
    case ImageCouldInitialize
}

///
/// The MovieAPI protocol, a high level interface for accessing the Movie Database API
///
protocol MovieAPI {
    
    func popularMovies(completionHandler: @escaping (Result<[MovieModel],MovieAPIError>) -> Void)

    func movieGenres(completionHandler: @escaping (Result<[GenreModel],MovieAPIError>) -> Void)
    
    func mediaObject(path:String, size:Int?, type:MediaObjectType, completionHandler: @escaping (Result<UIImage,MovieAPIError>) -> Void)
    
    func videosFromMovie(movieId:Int, completionHandler: @escaping (Result<[VideoModel],MovieAPIError>) -> Void)
}

///
/// Implementation of the MovieAPI protocol
///
class MovieAPIImplementation: MovieAPI {

    /// Retrieves popular movies
    ///
    /// This version does only retrieve page 1 in a paginated list
    func popularMovies(completionHandler handler: @escaping (Result<[MovieModel],MovieAPIError>) -> Void) {
        
        let url = Self.URL(apiurl:"https://api.themoviedb.org/3/movie/popular")
        
        let request = Self.URLRequest(url:url)
        
        HTTPNetworking.urlRequest(request) { (jsonResponse: Result<MoviesModel,HTTPNetworking.Error>) in
            
            assert(!Thread.current.isMainThread)
            
            DispatchQueue.main.async {
                
                switch jsonResponse {
                    case .success(let data):
                        handler(.success(data.results ?? []))
                    case .failure(let error):
                        handler(.failure(.HTTPGetRequest(error)))
                }
            }
        }
    }
    
    ///
    /// Retrieves movie genres in the database
    ///
    func movieGenres(completionHandler handler: @escaping (Result<[GenreModel],MovieAPIError>) -> Void) {
        
        let url = Self.URL(apiurl:"https://api.themoviedb.org/3/genre/movie/list")
        
        let request = Self.URLRequest(url:url)
        
        HTTPNetworking.urlRequest(request) { (jsonResponse: Result<GenresModel,HTTPNetworking.Error>) in
        
            DispatchQueue.main.async {
                
                switch jsonResponse {
                    case .success(let data):
                        handler(.success(data.genres ?? []))
                    case .failure(let error):
                        handler(.failure(.HTTPGetRequest(error)))
                }
            }
        }
    }
    
    ///
    /// Retrieve a media object (poster image)
    ///
    /// - Note: type parameter has not been tested for other than JPG
    ///
    func mediaObject(path:String, size:Int?, type:MediaObjectType, completionHandler handler:@escaping (Result<UIImage,MovieAPIError>) -> Void) {
        
        if type == .SVG && size != nil {
            handler(.failure(MovieAPIError.InvalidParameter("SVG format does not allow resizing")))
        }
        
        var newpath = path
        
        if newpath[newpath.startIndex] == "/" {
            newpath.removeFirst()
        }
        
        if newpath.contains(".") {
            newpath.removeLast(4)
        }
        
        let url = "https://image.tmdb.org/t/p/" + (size == nil ? "original" : "w" + String(size!))  + "/" + newpath + "." + type.filePathExtension
        
        // Do the job in a background thread
        DispatchQueue.global().async {
            
            if let url = Foundation.URL(string:url) {
            
                do {
                    
                    let data = try Data(contentsOf:url)
                    
                    if let image = UIImage(data:data) {
                        
                        DispatchQueue.main.async {
                            handler(.success(image))
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            handler(.failure(MovieAPIError.ImageCouldInitialize))
                        }
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        handler(.failure(MovieAPIError.MediaObjectLoad(error.localizedDescription)))
                    }
                }
            }
            else {
                assert(false)
            }
        }
    }
    
    ///
    /// Among other things, retrieves a YouTube id using the id of an movie
    ///
    func videosFromMovie(movieId:Int, completionHandler handler: @escaping (Result<[VideoModel],MovieAPIError>) -> Void) {
        
        let url = Self.URL(apiurl:"https://api.themoviedb.org/3/movie/" + String(movieId) + "/videos")
        
        let request = Self.URLRequest(url:url)
        
        HTTPNetworking.urlRequest(request) { (jsonResponse: Result<VideosModel,HTTPNetworking.Error>) in
        
            DispatchQueue.main.async {
                
                switch jsonResponse {
                    case .success(let data):
                        handler(.success(data.results ?? []))
                    case .failure(let error):
                        handler(.failure(.HTTPGetRequest(error)))
                }
            }
        }
    }
}


extension MovieAPIImplementation {
    
    fileprivate static let APIKey = "a6eb30752c8935dcae9e5b56df0a9d9f"
}

///
/// Utility functions used by the implementation of MovieAPI
///
extension MovieAPIImplementation {
    
    fileprivate static func URLRequest(url:URL) -> URLRequest {
        
        Foundation.URLRequest(url:url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: TimeInterval(10))
    }
    
    fileprivate static func URL(apiurl:String) -> URL {
        
        let string = apiurl + "?" + "api_key=" + Self.APIKey + "&language=en-US&page=1&Cache-Control=max-age=10"
        
        return Foundation.URL(string:string)!
    }
}

extension MediaObjectType {
    
    var filePathExtension: String {
        switch self {
            case .JPG: return "jpg"
            case .PNG: return "png"
            case .SVG: return "svg"
        }
    }
}

///
/// Extension that provides textual description of errors
///
extension MovieAPIError: LocalizedError {
    
    var errorDescription: String? {
        
        switch self {
        
            case let .InvalidParameter(string):
                return "Invalid parameter error: " + string
                
            case let .MediaObjectLoad(string):
                return "Error in load of Media Object - probably because URL cannot be read. Additional info: " + string
            
            case let .HTTPGetRequest(error):
                return error.localizedDescription
            
            case .ImageCouldInitialize:
                return "Data conversion error. Data could not be converted to an image."
        }
    }
}

