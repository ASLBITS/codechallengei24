//
//  MovieAPIMockup.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 22/06/2021.
//

import Foundation
import UIKit

/// Implementation of a MovieAPI mockup
class MovieAPIMockup1: MovieAPI {
    
    var moviesCount = 0
    
    init(moviesCount: Int) {
        self.moviesCount = moviesCount
    }

    func popularMovies(completionHandler handler: @escaping (Result<[MovieModel],MovieAPIError>) -> Void) {
        
        var movies = [MovieModel]()
        
        for i in 0..<moviesCount {

            movies.append(MovieModel(id:i, posterPath:"posterpath", title:"title\(i)",
                                     overview:"overview\(i)", releaseDate:"10/10/21", genres:[0,2]))
        }
        
        sleep(1)

        handler(.success(movies))
    }
    
    func movieGenres(completionHandler handler: @escaping (Result<[GenreModel],MovieAPIError>) -> Void) {
        
        sleep(1)
        
        handler(.success([GenreModel(id:0,name:"genre0"),GenreModel(id:1,name:"genre1"),GenreModel(id:2,name:"genre2")]))
    }
    
    func mediaObject(path:String, size:Int?, type:MediaObjectType, completionHandler handler:@escaping (Result<UIImage,MovieAPIError>) -> Void) {
        
        sleep(1)
        
        handler(.success(UIImage(named:"film")!))
    }
    
    func videosFromMovie(movieId:Int, completionHandler handler: @escaping (Result<[VideoModel],MovieAPIError>) -> Void) {
        
    }
}

/// Implementation of a MovieAPI mockup
class MovieAPIMockup2: MovieAPI {
    
    func popularMovies(completionHandler handler: @escaping (Result<[MovieModel],MovieAPIError>) -> Void) {
        
        handler(.failure(.HTTPGetRequest(HTTPNetworking.Error.RequestFailed("no internet"))))
    }
    
    func movieGenres(completionHandler handler: @escaping (Result<[GenreModel],MovieAPIError>) -> Void) {
        
        handler(.failure(.HTTPGetRequest(HTTPNetworking.Error.RequestFailed("no internet"))))
    }
    
    func mediaObject(path:String, size:Int?, type:MediaObjectType, completionHandler handler:@escaping (Result<UIImage,MovieAPIError>) -> Void) {
        
        handler(.failure(.HTTPGetRequest(HTTPNetworking.Error.RequestFailed("no internet"))))
    }
    
    func videosFromMovie(movieId:Int, completionHandler handler: @escaping (Result<[VideoModel],MovieAPIError>) -> Void) {
        
        handler(.failure(.HTTPGetRequest(HTTPNetworking.Error.RequestFailed("no internet"))))
    }
}
