//
//  AppDelegate.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 15/06/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Added the following code to delay launch screen (ASL)
        
        sleep(1)
        
        // Below code was inserted as replacement for using the storyboard to set up initial view controller. (ASL)
        
        let movieAPI = MovieAPIImplementation()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: CatalogViewController(movieAPI: movieAPI))
        window?.makeKeyAndVisible()
        
        // Other steps to know about when removing storyboards: (ASL)
        // https://medium.com/swift-productions/ios-start-a-project-without-storyboard-xcode-12-253d785af5e7

        return true
    }
}

