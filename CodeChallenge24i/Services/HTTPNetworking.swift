//
//  HTTPNetworking.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 21/06/2021.
//

import Foundation

///
/// HTTP request service
///
class HTTPNetworking {
    
    enum Error: Swift.Error {
        
        case RequestFailed(String)
        case Decoding(String)
        case BadHTTPCode(Int)
        
        case UnexpectedNoResponse
        case UnexpectedNoData
    }
    
    ///
    /// Returns JSON object in response to URL request
    ///
    static func urlRequest<JSONData:Codable>(_ request: URLRequest, completionHandler: @escaping (Result<JSONData,Error>) -> Void) {
        
        urlRequest(request) { (result: Result<Data,Error>) in
            
            switch result {
            
                case .success(let data):
                    
                    let decoder = JSONDecoder()
                    
                    do {

                        let json = try decoder.decode(JSONData.self, from:data)
                        
                        completionHandler(.success(json))
                    }
                    catch {
                        
                        completionHandler(.failure(.Decoding(error.localizedDescription)))
                    }

                case .failure(let error):
                    completionHandler(.failure(error))
            }
        }
    }
    
    ///
    /// Returns Foundation.Data object in response to URL request. Data object is embedded in a Result object
    ///
    static func urlRequest(_ request: URLRequest, completionHandler: @escaping (Result<Data,Error>) -> Void) {
        
        URLSession.shared.dataTask(with: request) { data, response, error in
                
            if let error = error {
                // Happens (verified) if network is disconnected and caching is off
                completionHandler(.failure(.RequestFailed(error.localizedDescription)))
                return
            }
                
            if let resp = response as? HTTPURLResponse {

                if resp.statusCode != 200 {
                    completionHandler(.failure(.BadHTTPCode(resp.statusCode)))
                    return
                }
            }
            else {
                    
                completionHandler(.failure(.UnexpectedNoResponse))
                return
            }
                
            guard data != nil else {
                    
                completionHandler(.failure(.UnexpectedNoData))
                return
            }
            
            completionHandler(.success(data!))
        }
        .resume()
    }
}

///
/// Extension that provides textual description of errors
///
extension HTTPNetworking.Error: LocalizedError {
    
    var errorDescription: String? {
        switch self {
            case let .RequestFailed(errorDescription): return "HTTP Request Failed: " + errorDescription
            case let .Decoding(errorDescription): return "Decoding error: " + errorDescription
            case let .BadHTTPCode(code): return HTTPCodeMessage(code)
            case .UnexpectedNoResponse: return "Unexpected Error in HTTP Request - no Response object"
            case .UnexpectedNoData: return "Unexpected Error in HTTP Request - no Data object"
        }
    }
    
    // Provides translation for some of the HTTP codes
    func HTTPCodeMessage(_ code: Int) -> String {
        
        var message: String?
        
        switch code {
            case 400: message = "Bad request" // probably because of syntax error in body or something like that
            case 403: message = "Forbidden" // request was understood, but perhaps something is wrong with access rights
        default:
            break
        }
        
        return "HTTP code (\(code)) error" + (message == nil ? "" : " - " + message!)
    }
}

