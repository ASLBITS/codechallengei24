//
//  VideoModel.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 19/06/2021.
//

import Foundation


struct VideoModel: Codable {
    let id: String?
    let key: String?
    let site: String?
    let name: String?
}
