//
//  GenresModel.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 18/06/2021.
//

import Foundation


struct GenresModel: Codable {
    let genres: [GenreModel]?
}

