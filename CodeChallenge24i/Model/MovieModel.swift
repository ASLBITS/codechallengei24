//
//  MovieModel.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 16/06/2021.
//

import Foundation


struct MovieModel: Codable {
    let id: Int
    let posterPath: String?
    let title: String?
    let overview: String?
    let releaseDate: String?
    let genres: [Int]?
    
}

extension MovieModel {
    
    ///
    /// Translate to camel casing
    ///
    enum CodingKeys: String, CodingKey {
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case genres = "genre_ids"
        case title, overview, id
    }
}
