//
//  MoviesModel.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 17/06/2021.
//


import Foundation


struct MoviesModel: Codable {
    let page: Int?
    let results: [MovieModel]?
    
}
