//
//  MoviesViewModel.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 22/06/2021.
//

import Foundation

/// View-model class encapsulating and representing a collection of movies and an API for network operations
class MoviesViewModel {
    
    var movies: [MovieViewModel]
    
    fileprivate let movieAPI: MovieAPI
    fileprivate var loadedMovies: [MovieViewModel]
    fileprivate var genres: [GenreModel]?
    
    init(movieAPI: MovieAPI) {
        
        self.movieAPI = movieAPI
        
        loadedMovies = []
        movies = []
    }
    
    /// Loads popular movies using the movieAPI. It works in two steps. First it loads all the movies and afterwards it updates all properties.
    ///
    /// - Parameters:
    ///     - loadHandler: handler that is called when all movies has been loaded (not all property may be loaded)
    ///     - updateHandler: handler that is called whenever a movie view-model object is updated (genres or poster-image)
    func loadMovies(loadHandler: @escaping (Error?) -> Void, updateHandler: @escaping (Result<MovieViewModel,Error>) -> Void) {
        
        movieAPI.movieGenres(completionHandler: { [weak self] result in
            
            switch result {
            
            case .failure(let error):
                loadHandler(error)
                
            case .success(let genres):
                self?.genres = genres
                
                self?.movieAPI.popularMovies(completionHandler: { [weak self] result in
                    
                    switch result {
                    
                    case .failure(let error):
                        loadHandler(error)
                        
                    case .success(let modelMovies):
                        self?.loadedMovies = modelMovies.map( { MovieViewModel(model:$0) })
                        self?.movies = self?.loadedMovies ?? []
                        loadHandler(nil)
                        
                        // Launch update of poster images
                        for movie in self?.movies ?? [] {
                            
                            var inhibite = false
                            
                            if let path = movie.posterImagePath {
                                
                                self?.movieAPI.mediaObject(path:path, size:300, type:.JPG, completionHandler: { result in
                                    
                                    assert(Thread.current.isMainThread)
                                    
                                    switch result {
                                    
                                    case .success(let image):
                                        movie.posterImage = image
                                        updateHandler(.success(movie))
                                        
                                    case .failure(let error):
                                        if !inhibite { updateHandler(.failure(error)) }
                                        inhibite = true
                                    }
                                })
                            }
                            
                            movie.initGenres(map: genres)

                            self?.movieAPI.videosFromMovie(movieId: movie.movieId, completionHandler: { result in
                                
                                switch result {
                                
                                case .success(let videos):
                                    movie.videoIdentifier = videos.first?.key
                                    updateHandler(.success(movie))
                                    
                                case .failure(let error):
                                    if !inhibite { updateHandler(.failure(error)) }
                                    inhibite = true
                                }
                            })
                            
                        } // for
                    }
                })
            }
        })
    }
    
    /// Filters the loaded movies with the specified text, testing whether text is contained in movie title
    func search(searchText:String) {
        
        guard !searchText.isEmpty else { return }

        movies = loadedMovies.filter( { $0.title.lowercased().contains(searchText.lowercased()) })
    }
    
    func searchCancel() {
        
        movies = loadedMovies
    }
}

