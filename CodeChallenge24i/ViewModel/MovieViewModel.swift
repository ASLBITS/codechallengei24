//
//  MovieViewModel.swift
//  CodeChallenge24i
//
//  Created by Anders Lassen on 22/06/2021.
//

import Foundation
import UIKit

/// View-model class represententing a movie (model)
class MovieViewModel {
    
    let title: String
    let releaseDate: String
    let overview: String
    
    /// Late initialized properties
    var posterImage: UIImage?
    var genres: [String]?
    var videoIdentifier: String?
    
    /// Access to model properties
    var posterImagePath: String? {
        model.posterPath
    }
    
    var movieId: Int {
        model.id
    }

    fileprivate var model:MovieModel
    
    init(model: MovieModel) {
        
        self.model = model
        
        self.title = model.title ?? "No title"
        self.releaseDate = model.releaseDate ?? "No release date"
        self.overview = model.overview ?? "No overview"
    }
    
    /// Converts the object's genre ids to string representation using the provided map
    func initGenres(map:[GenreModel]) {
        
        genres = []
        
        for genre in model.genres ?? [] {
            
            if let name = map.first(where: { $0.id == genre })?.name {
                genres!.append(name)
            }
        }
    }
}
