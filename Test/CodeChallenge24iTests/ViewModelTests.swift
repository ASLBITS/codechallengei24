//
//  CodeChallenge24iTests.swift
//  CodeChallenge24iTests
//
//  Created by Anders Lassen on 22/06/2021.
//

import XCTest
@testable import CodeChallenge24i

/// A collection of tests that tests the two view-model classes
class ViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    /// Test movie view model's load of movies using API mockup with an simulated numbers of movies
    func testLoadMovies() throws {
        
        let moviesCount = 2

        let vm = MoviesViewModel(movieAPI:MovieAPIMockup1(moviesCount:moviesCount))
        
        let expectation = expectation(description:"load expection")
        
        expectation.expectedFulfillmentCount = moviesCount + 1

        vm.loadMovies(loadHandler: { error in
            XCTAssert(error == nil)
            expectation.fulfill()
        }, updateHandler: { result in
            switch result {
            case .failure(_):
                XCTAssert(false)
            case .success(_):
                expectation.fulfill()
                XCTAssert(true)
            }
        })
        
        waitForExpectations(timeout:20) { error in

            if let error = error {
                
                XCTFail("testLoadMovies:waitForExpectationsWithTimeout error: \(error)")
            }
            else {
                
                XCTAssert(vm.movies.count == moviesCount)
                
                for (i,movie) in vm.movies.enumerated() {
                    XCTAssert(movie.movieId == i)
                    XCTAssert(movie.title == "title\(i)")
                    XCTAssert(movie.overview == "overview\(i)")
                    XCTAssert(movie.posterImage != nil)
                    XCTAssert(movie.genres == ["genre0","genre2"])
                }
                
                vm.search(searchText:"title8")
                XCTAssert(vm.movies.count == 0)
                
                vm.search(searchText:"title1")
                XCTAssert(vm.movies.count == 1)
                
                vm.search(searchText:"title0")
                XCTAssert(vm.movies.count == 1)
                
                vm.searchCancel()
                XCTAssert(vm.movies.count == moviesCount)
            }
        }
    }
    
    /// Test movie view model's load of movies using API mockup with an simulated numbers of movies
    func testLoadMoviesWithError() throws {
        
        let vm = MoviesViewModel(movieAPI:MovieAPIMockup2())
        
        let expectation = expectation(description:"load expection")
        
        expectation.expectedFulfillmentCount = 1

        vm.loadMovies(loadHandler: { error in
            XCTAssert(error != nil)
            expectation.fulfill()
        }, updateHandler: { result in
            switch result {
            case .failure(_):
                XCTAssert(false)
                expectation.fulfill()
            case .success(_):
                expectation.fulfill()
                XCTAssert(false)
            }
        })
        
        waitForExpectations(timeout:20) { error in

            if let error = error {
                
                XCTFail("testLoadMovies:waitForExpectationsWithTimeout error: \(error)")
            }
            else {
                
                XCTAssert(vm.movies.count == 0)
            }
        }
    }
}
